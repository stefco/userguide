Glossary
========

.. glossary::

    BBH
        Binary black hole system, a binary system composed of two black holes.

    BNS
        Binary neutron star, a binary system composed of two neutron stars.

    MassGap
        Compact binary systems with at least one compact object whose mass is 
        in the hypothetical "mass gap" between neutron stars and black holes, 
        defined here as 3-5 solar masses.

    CBC
        Compact binary coalescence.

    FITS
        Flexible Image Transport System, a format for astronomical tables,
        images, and multidimensional data sets.

    LHO
        LIGO Hanford Observatory (see `LHO observatory home page
        <https://www.ligo.caltech.edu/LA>`_), site of a 4 km gravitational-wave
        detector in Hanford, Washington, USA.

    LLO
        LIGO Livingston Observatory (see `LLO observatory home page
        <https://www.ligo.caltech.edu/WA>`_), site of a 4 km gravitational-wave
        detector in Livingston, Louisiana, USA.

    GCN
        The Gamma-ray Coordinates Network (https://gcn.gsfc.nasa.gov), a portal
        for discoveries and observations of astronomical transients.
        Historically, GCN has served high-energy satellites but now also other
        electromagnetic wavelengths and also gravitational-wave, cosmic ray,
        and neutrino facilities.

    GCN Circular
        A human-readable astronomical bulletin distributed through :term:`GCN`.

    GCN Notice
        A machine-readable alert distributed through :term:`GCN`.

    GraceDb
        Gravitational Wave Candidate Event Database (https://gracedb.ligo.org),
        the official public marshal portal for LIGO/Virgo candidates.

    HEALPix
        Hierarchical Equal Area isoLatitude Pixelation, a scheme for indexing
        positions on the unit sphere.

    HEN
        High Energy Neutrino, particularly in the context of multi-messenger
        GW+HEN follow-up.

    NSBH
        Neutron star black hole, a binary system composed of one neutron star
        and one black hole.

    Virgo
        Virgo Observatory (see `Virgo observatory home page
        <http://www.virgo-gw.eu>`_), site of a 3 km gravitational-wave detector
        in Cascina, Italy.

    VOEvent
        An XML format for describing astronomical transients. For the
        specification, see the official `VOEvent IVOA Recommendation
        <http://www.ivoa.net/documents/VOEvent/index.html>`_.

    VTP
        VOEvent Transport Protocol, a simple TCP-based protocol for sending and
        receiving VOEvents, used by :term:`GCN`. For the specification, see the
        official `VTP IVOA recommendation
        <http://www.ivoa.net/documents/Notes/VOEventTransport/>`_.
